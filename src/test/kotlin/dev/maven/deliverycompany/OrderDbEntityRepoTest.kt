package dev.maven.deliverycompany

import dev.maven.deliverycompany.database.order.OrderDbEntity
import dev.maven.deliverycompany.database.order.OrderDbEntityRepository
import dev.maven.deliverycompany.database.order.ProductDbEntity
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class OrderDbEntityRepoTest (
    @Autowired private val orderDbEntityRepository: OrderDbEntityRepository
) {
    @Test
    fun printAllOrdersWithProducts() {
        val orders = orderDbEntityRepository.findAll()
        println("\n")
        orders.forEach {
            order ->
                order.products.forEach {
                    product -> println("Order #${order.orderId}: ${product.name} $${product.price} x${product.quantity} items")
                }
        }
        println("\n")
    }

    @Test
    fun saveOrderWithProducts() {
        val order = OrderDbEntity(
            products = listOf(
                ProductDbEntity(name = "Milk", price = 5.99, quantity = 2),
                ProductDbEntity(name = "Bread", price = 2.99, quantity = 1)
            )
        )

        orderDbEntityRepository.save(order)
    }
}
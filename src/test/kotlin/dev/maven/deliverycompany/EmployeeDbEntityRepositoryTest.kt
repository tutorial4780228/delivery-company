package dev.maven.deliverycompany

import dev.maven.deliverycompany.database.employee.EmployeeDbEntity
import dev.maven.deliverycompany.database.employee.EmployeeDbEntityRepository
import dev.maven.deliverycompany.database.employee.Name
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class EmployeeDbEntityRepositoryTest (
    @Autowired private val employeeDbEntityRepository: EmployeeDbEntityRepository
) {
    @Test
    fun testSavingEmployee() {
        val employeeDbEntity = EmployeeDbEntity(
            name = Name(first = "Sam", last = "Smith"),
            position = "Manager",
            salary = 5_500.00
        )

        employeeDbEntityRepository.save(employeeDbEntity)
    }

    @Test
    fun savePartnership() {
        val partner1 = EmployeeDbEntity(
            name = Name(first = "Blade", last = "Wolfmoon"),
            position = "IT-Spec",
            salary = 5_700.00
        )

        val partner2 = EmployeeDbEntity(
            name = Name(first = "John", last = "Cena"),
            position = "HR Specialist",
            salary = 4_000.00
        )

        partner1.partners.add(partner2)
        employeeDbEntityRepository.save(partner1)
    }

    @Test
    fun getPartnerships() {
        val employees = employeeDbEntityRepository.findAll()
        println()
        employees.forEach {
            employee -> employee.partners.forEach {
                partner -> println("${employee.name.first} ${employee.name.last} " +
                   "is a partner of ${partner.name.first} ${partner.name.last}")
            }
        }
        println()
    }
}
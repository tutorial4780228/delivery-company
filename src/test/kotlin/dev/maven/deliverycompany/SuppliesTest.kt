package dev.maven.deliverycompany

import dev.maven.deliverycompany.database.order.OrderDbEntity
import dev.maven.deliverycompany.database.order.ProductDbEntity
import dev.maven.deliverycompany.database.supplies.SupplierDbEntity
import dev.maven.deliverycompany.database.supplies.SuppliesDbEntity
import dev.maven.deliverycompany.database.supplies.SuppliesDbEntityRepo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class SuppliesTest (
    @Autowired private val suppliesDbEntityRepo: SuppliesDbEntityRepo
) {
    @Test
    fun addSupplies() {
        val supply = SuppliesDbEntity(
            quantity = 5,
            unitPrice = 50.0,
            supplier = SupplierDbEntity(name = "sam", address = "999 Highway", contactsDetails = "+79785545689"),
            orders = listOf(
                OrderDbEntity(products = listOf(
                    ProductDbEntity(name = "Milk", price = 5.99, quantity = 2),
                    ProductDbEntity(name = "Bread", price = 2.99, quantity = 1)
                    )
                )
            )
        )

        suppliesDbEntityRepo.save(supply)
    }

    @Test
    fun printAllSupplies () {
        val supplies = suppliesDbEntityRepo.findAll()

        supplies.forEach {
            println(it)
        }
    }
}
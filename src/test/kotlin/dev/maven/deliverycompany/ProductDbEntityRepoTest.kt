package dev.maven.deliverycompany

import dev.maven.deliverycompany.database.order.OrderDbEntity
import dev.maven.deliverycompany.database.order.ProductDbEntity
import dev.maven.deliverycompany.database.order.ProductDbEntityRepository
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class ProductDbEntityRepoTest (
    @Autowired private val productDbEntityRepository: ProductDbEntityRepository
) {
    @Test
    fun addProductWithOrder() {
        val order = OrderDbEntity()

        val products = listOf(
            ProductDbEntity(name = "Milk", price = 5.99, quantity = 2, order = order),
            ProductDbEntity(name = "Bread", price = 2.99, quantity = 1, order = order)
        )

        productDbEntityRepository.saveAll(products)
    }
}
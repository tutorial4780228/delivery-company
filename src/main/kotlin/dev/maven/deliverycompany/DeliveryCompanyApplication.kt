package dev.maven.deliverycompany

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DeliveryCompanyApplication

fun main(args: Array<String>) {
    runApplication<DeliveryCompanyApplication>(*args)
}

package dev.maven.deliverycompany.database.order

import dev.maven.deliverycompany.database.supplies.SuppliesDbEntity
import jakarta.persistence.*

@Entity
@Table(name = "orders")
class OrderDbEntity (
    @Id
    @SequenceGenerator(name = "order_id_sequence_generator", sequenceName = "order_id_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_id_sequence_generator")
    var orderId: Long = 0L,

    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id", referencedColumnName = "orderId")
    var products: List<ProductDbEntity> = emptyList(),

    @ManyToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "supply_id", referencedColumnName = "id")
    var supply: SuppliesDbEntity? = null
) {
    override fun toString(): String {
        return "OrderDbEntity(orderId=$orderId, products=$products)"
    }
}
package dev.maven.deliverycompany.database.order

import org.springframework.data.jpa.repository.JpaRepository

interface ProductDbEntityRepository : JpaRepository<ProductDbEntity, Long>
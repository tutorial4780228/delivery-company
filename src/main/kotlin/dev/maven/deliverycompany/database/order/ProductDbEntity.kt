package dev.maven.deliverycompany.database.order

import jakarta.persistence.*

@Entity
@Table(name = "product")
data class ProductDbEntity (
    @Id
    @SequenceGenerator(name = "product_id_sequence_generator", sequenceName = "product_id_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_id_sequence_generator")
    var productId: Long = 0L,

    var name: String = "",
    var price: Double = 0.0,
    var quantity: Int = 0,

    @ManyToOne(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id", referencedColumnName = "orderId")
    var order: OrderDbEntity? = null
) {
    override fun toString(): String {
        return "ProductDbEntity(productId=$productId, name='$name', price=$price, quantity=$quantity)"
    }
}
package dev.maven.deliverycompany.database.employee

import jakarta.persistence.*

@Entity
@Table(name = "employee")
data class EmployeeDbEntity (
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0L,

    @Embedded
    var name: Name = Name(),

    var position: String = "",
    var salary: Double = 0.0,

    @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    @JoinTable(
        name = "partnership",
        joinColumns = [
            JoinColumn(name = "employee_1_id", referencedColumnName = "id", foreignKey = ForeignKey(name = "partner_1"))
        ],
        inverseJoinColumns = [
            JoinColumn(name = "employee_2_id", referencedColumnName = "id", foreignKey = ForeignKey(name = "partner_2"))
        ]
    )
    var partners: MutableList<EmployeeDbEntity> = mutableListOf()
)
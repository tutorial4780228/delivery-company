package dev.maven.deliverycompany.database.employee

import jakarta.persistence.AttributeOverride
import jakarta.persistence.AttributeOverrides
import jakarta.persistence.Column
import jakarta.persistence.Embeddable

@Embeddable
@AttributeOverrides(
    AttributeOverride(name = "first", column = Column(name = "first_name")),
    AttributeOverride(name = "last", column = Column(name = "last_name"))
)
data class Name (
    var first: String = "",
    var last: String = ""
)
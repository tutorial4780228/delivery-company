package dev.maven.deliverycompany.database.employee

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface EmployeeDbEntityRepository : JpaRepository<EmployeeDbEntity, Long>
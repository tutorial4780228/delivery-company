package dev.maven.deliverycompany.database.supplies

import dev.maven.deliverycompany.database.order.OrderDbEntity
import jakarta.persistence.*

@Entity
@Table(name = "supply")
data class SuppliesDbEntity (
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0L,

    var quantity: Int = 0,
    var unitPrice: Double = 0.0,

    @ManyToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "supplier_id", referencedColumnName = "id")
    var supplier: SupplierDbEntity? = null,

    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    @JoinColumn(name = "supply_id", referencedColumnName = "id")
    var orders: List<OrderDbEntity> = emptyList()
)
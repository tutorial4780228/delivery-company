package dev.maven.deliverycompany.database.supplies

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SuppliesDbEntityRepo : JpaRepository<SuppliesDbEntity, Long>
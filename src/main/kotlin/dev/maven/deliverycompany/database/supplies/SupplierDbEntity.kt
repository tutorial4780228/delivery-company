package dev.maven.deliverycompany.database.supplies

import jakarta.persistence.*

@Entity
@Table(name = "supplier")
class SupplierDbEntity (
    @Id
    @GeneratedValue
    var id: Long = 0L,

    var name: String = "",
    var address: String = "",
    var contactsDetails: String = "",

    @OneToMany(cascade = [CascadeType.ALL])
    @JoinColumn(name = "supplier_id", referencedColumnName = "id")
    var supplies: List<SuppliesDbEntity> = emptyList()
) {
    override fun toString(): String {
        return "SupplierDbEntity(id=$id, name='$name', address='$address', contactsDetails='$contactsDetails')"
    }
}